import { configure, addDecorator } from "@storybook/react";
import { withConsole } from "@storybook/addon-console";
import { INITIAL_VIEWPORTS } from "@storybook/addon-viewport";
import "@storybook/addon-console";
import { addParameters } from "@storybook/react";

addParameters({ viewport: { viewports: INITIAL_VIEWPORTS } });
// automatically import all files ending in *.stories.js
function loadStories() {
  const context = require.context("../src", true, /\-story.js$/);
  const files = context.keys();

  files.forEach(filename => {
    context(filename);
  });
}

addDecorator((storyFn, context) => withConsole()(storyFn)(context));

configure(loadStories, module);
