import React from "react";
import { addDecorator, addParameters } from "@storybook/react";
import { withInfo } from "@storybook/addon-info";
import { withConsole } from "@storybook/addon-console";
import { INITIAL_VIEWPORTS } from "@storybook/addon-viewport";
import { View, StyleSheet } from "react-native";
import { withPerformance } from "storybook-addon-performance";

addDecorator(withInfo({ inline: true }));
addDecorator((storyFn, context) => withConsole()(storyFn)(context));
addDecorator(withPerformance);
const Wrapper = ({ children }) => {
  const styles = StyleSheet.create({
    base: {
      backgroundColor: "#2b1a48",
    },
  });
  return <View style={styles.base}>{children}</View>;
};
addDecorator(storyFn => <Wrapper>{storyFn()}</Wrapper>);

addParameters({
  viewport: {
    viewports: INITIAL_VIEWPORTS,
  },
});
