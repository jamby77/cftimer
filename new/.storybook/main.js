module.exports = {
  stories: ["../src/**/*-story.js"],
  addons: [
    "@storybook/addon-actions",
    "@storybook/addon-backgrounds/register",
    "@storybook/addon-links",
    "@storybook/addon-viewport/register",
    "@storybook/preset-create-react-app",
    "storybook-addon-performance/register",
  ],
};
