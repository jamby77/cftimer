import React, { useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import { TimerType } from "./stateMachines/timerMachine";
import TimerSelector from "./components/TimerSelector";
const { StopWatch, CountDown } = TimerType;

export default function Setup() {
  const [type, setType] = useState(null);

  return (
    <View>
      <Text style={styles.title}>Choose a timer</Text>
      <TimerSelector
        onPress={() => setType(StopWatch)}
        type={StopWatch}
        selected={type === StopWatch}
      >
        StopWatch / For time
      </TimerSelector>
      <TimerSelector
        onPress={() => setType(CountDown)}
        type={CountDown}
        selected={type === CountDown}
      >
        Countdown / AMRAP
      </TimerSelector>
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: "4em",
    textAlign: "center",
    textTransform: "uppercase",
    color: "#ffffff",
  },
});
