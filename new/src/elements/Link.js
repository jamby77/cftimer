import React from "react";
import { Text, Linking, StyleSheet } from "react-native";
export const Link = ({ href, style, children }) => {
  return (
    <Text
      accessibilityRole="link"
      href={href}
      onPress={() => {
        Linking.openURL(href);
      }}
      style={StyleSheet.compose(styles.link, style)}
    >
      <Text>{children}</Text>
    </Text>
  );
};
const styles = StyleSheet.create({
  link: {
    color: "#1B95E0",
    textDecorationLine: "underline",
  },
});

export default Link;
