import React from "react";
import { TouchableHighlight, Text, View, StyleSheet } from "react-native";
export const Button = ({ style, btnLabelStyle, title, onPress, ...rest }) => {
  return (
    <TouchableHighlight
      accessibilityRole="button"
      style={StyleSheet.compose(styles.btnWrapper, style)}
      onPress={onPress}
    >
      <View>
        <Text style={StyleSheet.compose(styles.btn, btnLabelStyle)} {...rest}>
          {title || rest.children}
        </Text>
      </View>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  btn: {
    textTransform: "uppercase",
    textAlign: "center",
    fontWeight: 500,
    color: "#ffffff",
    padding: "8px",
  },
  btnWrapper: {
    transition: "opacity 0.15s",
    userSelect: "none",
    cursor: "pointer",
    borderRadius: "2px",
    backgroundColor: "rgb(33, 150, 243)",
  },
});

export default Button;
