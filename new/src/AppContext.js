import React, {
  createContext,
  useReducer,
  useCallback,
  useLayoutEffect,
} from "react";
import { Dimensions } from "react-native";

const { height, width } = Dimensions.get("window");
const setVertical = (w, h) => {
  return h >= w || Math.abs(w - h) < 150;
};
export const displayForm = "form",
  displayTimer = "timer";

const initialState = {
  workout: "",
  timer: null,
  display: displayForm,
  dispatch: action => undefined,
  isVertical: setVertical(width, height),
};

const timer = "timer",
  workout = "workout",
  display = "display",
  orientation = "orientation";

export const AppActions = {
  timer,
  workout,
  display,
  orientation,
};

export const AppContext = createContext(initialState);

const reducer = (state, action) => {
  console.log(action);

  const { type, data } = action;
  let newState;
  switch (type) {
    case workout:
      newState = { ...state, workout: data };
      break;
    case timer:
      newState = { ...state, timer: data };
      break;
    case display:
      newState = { ...state, display: data };
      break;
    case orientation:
      newState = { ...state, isVertical: data };
      break;
    default:
      newState = state;
  }
  return newState;
};

export function AppContextComponent({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const listener = useCallback(
    ({ window: appView }) => {
      const isVerticalApp = setVertical(appView.width, appView.height);

      if (state.isVertical !== isVerticalApp) {
        dispatch({ type: orientation, data: isVerticalApp });
      }
    },
    [state],
  );
  useLayoutEffect(() => {
    Dimensions.addEventListener("change", listener);
    return () => {
      Dimensions.removeEventListener("change", listener);
    };
  }, [listener]);
  const context = {
    ...state,
    dispatch,
  };
  return <AppContext.Provider value={context}>{children}</AppContext.Provider>;
}

export default AppContextComponent;
