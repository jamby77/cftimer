import React from "react";
import AppContextComponent from "./AppContext";
import Home from "./Home";

function App() {
  return (
    <AppContextComponent>
      <Home />
    </AppContextComponent>
  );
}

export default App;
