import React, { useContext } from "react";
import { View, StyleSheet } from "react-native";
import { AppContext, displayTimer, displayForm } from "./AppContext";
import Setup from "./Setup";
import Workout from "./Workout";

function Home() {
  const { display } = useContext(AppContext);
  return (
    <View style={styles.home}>
      {display === displayForm && <Setup />}
      {display === displayTimer && <Workout />}
    </View>
  );
}
const styles = StyleSheet.create({
  home: { justifyContent: "center", height: "100%" },
});

export default Home;
