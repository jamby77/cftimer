import React, { useContext } from "react";
import { View, StyleSheet } from "react-native";
import WorkoutDescription from "./components/WorkoutDescription";
import Timer from "./components/Timer";
import { timerMachine } from "./stateMachines/timerMachine";
import { AppContext } from "./AppContext";

export default function Workout() {
  const { workout, timer, isVertical } = useContext(AppContext);
  const concreteMachine = timerMachine.withContext({
    ...timerMachine.context,
    timers: [{ ...timer, timerType: timer.type }],
    countdown: timer.countdown + 1,
  });

  const hasWorkout = typeof workout === "string" && workout.trim().length > 0;
  let s = styles.layoutVertical;
  if (!isVertical && hasWorkout) {
    s = styles.layoutHorizontal;
  }
  return (
    <View style={s}>
      {hasWorkout && (
        <WorkoutDescription workout={workout} isVertical={isVertical} />
      )}
      <Timer machine={concreteMachine} isVertical={isVertical || !hasWorkout} />
    </View>
  );
}

const styles = StyleSheet.create({
  layoutVertical: {
    flexDirection: "column",
    alignItems: "center",
  },
  layoutHorizontal: {
    flexDirection: "row",
  },
});
