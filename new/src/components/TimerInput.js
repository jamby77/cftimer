import React from "react";
import { View, StyleSheet, Text } from "react-native";
import Button from "../elements/Button";

const maxTime = (99 * 60 + 99) * 1000;
const parseTime = time => {
  let min = 0,
    sec = 0,
    minDec = 0,
    secDec = 0;
  if (time && time > 0) {
    const timeInSeconds = time / 1000;
    const seconds = timeInSeconds % 60;
    secDec = Math.floor(seconds / 10);
    sec = seconds % 10;
    const minutes = Math.min(Math.floor(timeInSeconds / 60), 99);
    minDec = Math.floor(minutes / 10);
    min = minutes % 10;
  }
  return { min, minDec, sec, secDec };
};
const aSec = 1000;
const aMin = aSec * 60;

export default function TimerInput({
  time = 0,
  onChange = time => console.log(time),
}) {
  const { min, minDec, sec, secDec } = parseTime(time);

  const updateTime = timeToUpdate => {
    const update = Math.max(Math.min(timeToUpdate, maxTime), 0);
    onChange(update);
  };

  const secDecInc = () => {
    updateTime(time + 10 * aSec); // increase time with 10 seconds
  };
  const secInc = () => {
    updateTime(time + aSec); // increase time with 1 second
  };
  const minDecInc = () => {
    updateTime(time + 10 * aMin); // increase time with 10 minutes 10 * 60 * 1000
  };
  const minInc = () => {
    updateTime(time + aMin); // increase time with 1 minute 1*60*1000
  };

  const secDecDec = () => {
    updateTime(time - 10 * aSec); // Decrease time with 10 seconds
  };
  const secDecHandler = () => {
    updateTime(time - aSec); // Decrease time with 1 second
  };
  const minDecDec = () => {
    updateTime(time - 10 * aMin); // Decrease time with 10 minutes 10 * 60 * 1000
  };
  const minDecHandler = () => {
    updateTime(time - aMin); // Decrease time with 1 minute 1*60*1000
  };
  return (
    <View style={styles.row}>
      <View style={styles.minutes}>
        <View>
          <Button
            style={[styles.btn, styles.btnUp]}
            btnLabelStyle={[styles.digits, styles.short]}
            onPress={minDecInc}
          >
            +
          </Button>
          <Text style={[styles.digits, styles.inverted]}>{minDec}</Text>
          <Button
            style={[styles.btn, styles.btnDown]}
            btnLabelStyle={[styles.digits, styles.short]}
            onPress={minDecDec}
          >
            -
          </Button>
        </View>
        <View>
          <Button
            style={[styles.btn, styles.btnUp]}
            btnLabelStyle={[styles.digits, styles.short]}
            onPress={minInc}
          >
            +
          </Button>
          <Text style={[styles.digits, styles.inverted]}>{min}</Text>
          <Button
            style={[styles.btn, styles.btnDown]}
            btnLabelStyle={[styles.digits, styles.short]}
            onPress={minDecHandler}
          >
            -
          </Button>
        </View>
      </View>
      <Text style={styles.digits}> : </Text>
      <View style={styles.seconds}>
        <View>
          <Button
            style={[styles.btn, styles.btnUp]}
            btnLabelStyle={[styles.digits, styles.short]}
            onPress={secDecInc}
          >
            +
          </Button>
          <Text style={[styles.digits, styles.inverted]}>{secDec}</Text>
          <Button
            style={[styles.btn, styles.btnDown]}
            btnLabelStyle={[styles.digits, styles.short]}
            onPress={secDecDec}
          >
            -
          </Button>
        </View>
        <View>
          <Button
            style={[styles.btn, styles.btnUp]}
            btnLabelStyle={[styles.digits, styles.short]}
            onPress={secInc}
          >
            +
          </Button>
          <Text style={[styles.digits, styles.inverted]}>{sec}</Text>
          <Button
            style={[styles.btn, styles.btnDown]}
            btnLabelStyle={[styles.digits, styles.short]}
            onPress={secDecHandler}
          >
            -
          </Button>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  digits: {
    fontSize: "3em",
    color: "#fff",
  },
  inverted: {
    backgroundColor: "#ffffff",
    color: "#000000",
  },
  short: { paddingTop: "0", maxHeight: "50px" },
  btn: {
    borderRadius: "0",
  },
  btnUp: {
    borderTopEndRadius: "50%",
    borderTopStartRadius: "50%",
  },
  btnDown: {
    borderBottomEndRadius: "50%",
    borderBottomStartRadius: "50%",
  },
  minutes: {
    flexDirection: "row",
  },
  seconds: {
    flexDirection: "row",
  },
});
