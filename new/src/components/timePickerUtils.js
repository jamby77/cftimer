import { StyleSheet } from "react-native";

const maxMinutes = 100;
const maxSeconds = 60;
const minutes = new Array(maxMinutes);
const seconds = new Array(maxSeconds);
minutes.fill(0);
seconds.fill(0);

const styles = StyleSheet.create({
  container: {
    borderWidth: 2,
    borderColor: "transparent",
    padding: 10,
    alignItems: "center",
    boxShadow: "1px 1px 1px 1px rgba(0, 0, 0, 0.2)",
  },
  picker: {
    flexDirection: "row",
    paddingTop: 10,
    paddingBottom: 10,
  },
  pickerItem: {
    flex: 1,
    flexBasis: "7rem",
    flexGrow: 0,
    fontSize: "4rem",
  },
});

const maxTime = (99 * 60 + 99) * 1000;

const parseTime = time => {
  let minutes = 0,
    seconds = 0;
  if (time && time > 0) {
    const timeInSeconds = time / 1000;
    seconds = timeInSeconds % 60;
    minutes = Math.min(Math.floor(timeInSeconds / 60), 99);
  }
  return [minutes, seconds];
};

export default { minutes, seconds, parseTime, maxTime, styles };
