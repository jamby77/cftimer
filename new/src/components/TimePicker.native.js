import React, { useState, useEffect } from "react";
import MultiPicker from "rmc-picker/es/MultiPicker.native";
import Picker from "rmc-picker/es/Picker.native";
import { View } from "react-native";

import utils from "./timePickerUtils";

const { parseTime, maxTime, styles, minutes, seconds } = utils;

const TimePicker = ({ onChange = time => undefined, time = 0 }) => {
  const [value, setValue] = useState(parseTime(time));

  useEffect(() => {
    const timeToUpdate = (value[0] * 60 + value[1]) * 1000; // minutes plus seconds to milliseconds
    if (timeToUpdate === time) {
      return;
    }
    const update = Math.max(Math.min(timeToUpdate, maxTime), 0);
    onChange(update);
  }, [onChange, time, value]);

  return (
    <View style={styles.container}>
      <MultiPicker
        selectedValue={value}
        style={styles.picker}
        onValueChange={newValue => {
          setValue(newValue.map(Number));
        }}
      >
        <Picker
          style={styles.pickerItem}
          onValueChange={(...args) => console.log(args)}
        >
          {minutes.map((_, idx) => {
            return (
              <Picker.Item key={idx} value={idx}>
                {`${idx.toString().padStart(2, "0")} m`}
              </Picker.Item>
            );
          })}
        </Picker>
        <Picker
          style={styles.pickerItem}
          onValueChange={(...args) => console.log(args)}
        >
          {seconds.map((_, idx) => {
            return (
              <Picker.Item key={idx} value={idx}>
                {`${idx.toString().padStart(2, "0")} s`}
              </Picker.Item>
            );
          })}
        </Picker>
      </MultiPicker>
    </View>
  );
};

export default TimePicker;
