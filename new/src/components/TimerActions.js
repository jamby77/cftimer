import React, { useContext } from "react";
import { View, StyleSheet } from "react-native";
import Button from "../elements/Button";
import { timerStates, timerTransitions } from "../stateMachines/timerMachine";
import { AppContext, AppActions, displayForm } from "../AppContext";

export default function TimerActions({ state, send, isVertical = true }) {
  const { dispatch } = useContext(AppContext);
  const textStyle = isVertical ? styles.btnText : styles.btnTextHorizontal;
  return (
    <View style={styles.row}>
      {state.matches(timerStates.idle) && [
        <Button
          btnLabelStyle={textStyle}
          key="start"
          style={[styles.flexed, styles.buttonStart]}
          onPress={() => send(timerTransitions.START)}
          title="Start"
        />,
        <Button
          btnLabelStyle={textStyle}
          style={[styles.flexed]}
          key="done"
          onPress={() =>
            dispatch({ type: AppActions.display, data: displayForm })
          }
          title="Back"
        />,
      ]}
      {state.matches(timerStates.running) && [
        <Button
          btnLabelStyle={textStyle}
          style={[styles.flexed]}
          key="pause"
          onPress={() => send(timerTransitions.PAUSE)}
          title="Pause"
        />,
        <Button
          btnLabelStyle={textStyle}
          style={[styles.flexed, styles.buttonStop]}
          key="stop"
          onPress={() => send(timerTransitions.STOP)}
          title="Stop"
        />,
      ]}
      {state.matches(timerStates.paused) && [
        <Button
          btnLabelStyle={textStyle}
          style={[styles.flexed, styles.buttonStart]}
          key="resume"
          onPress={() => send(timerTransitions.START)}
          title="Resume"
        />,
        <Button
          btnLabelStyle={textStyle}
          style={[styles.flexed, styles.buttonStop]}
          key="reset"
          buttonStyle={styles.container}
          onPress={() => send(timerTransitions.RESET)}
          title="Reset"
        />,
      ]}
      {state.matches(timerStates.finished) && [
        <Button
          btnLabelStyle={textStyle}
          style={[styles.flexed, styles.buttonStop]}
          key="reset"
          buttonStyle={styles.container}
          onPress={() => send(timerTransitions.RESET)}
          title="Reset"
        />,
        <Button
          btnLabelStyle={textStyle}
          style={[styles.flexed]}
          key="done"
          onPress={() =>
            dispatch({ type: AppActions.display, data: displayForm })
          }
          title="Done"
        />,
      ]}
    </View>
  );
}

const styles = StyleSheet.create({
  flexed: { flex: 1 },
  container: {
    alignItems: "center",
    justifyContent: "center",
  },
  row: {
    flexDirection: "row",
  },
  btnText: {
    fontSize: "5vw",
  },
  btnTextHorizontal: {
    fontSize: "2.5vw",
  },
  buttonStart: {
    backgroundColor: "#23d388",
  },
  buttonStop: {
    backgroundColor: "#e53e3e",
  },
});
