import React from "react";
import { Text, StyleSheet, View } from "react-native";

export default function WorkoutDescription({ workout, isVertical = true }) {
  return (
    <View style={[styles.container, !isVertical && styles.containerHorizontal]}>
      <Text
        style={[
          isVertical ? styles.workoutVertical : styles.workoutHorizontal,
          styles.text,
        ]}
      >
        {workout}
      </Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    overflow: "auto",
    flex: 1,
    maxHeight: "80%",
  },
  containerHorizontal: {
    maxWidth: "50%",
    maxHeight: "100%",
  },
  text: {
    color: "white",
    whiteSpace: "pre-line",
    padding: "1vw",
  },
  workoutVertical: {
    fontSize: "3vw",
  },
  workoutHorizontal: {
    fontSize: "2vw",
  },
});
