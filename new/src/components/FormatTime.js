import React from "react";
import PropTypes from "prop-types";
import { View, Text, StyleSheet } from "react-native";

const Sep = ({ children = ":", style }) => (
  <Text style={[styles.timePart, style]}>{children}</Text>
);

Sep.propTypes = {
  children: PropTypes.string.isRequired,
};

const FormatTime = ({ time = 0, isVertical = true }) => {
  // time is in milliseconds
  const h = Math.floor(time / 3600000);
  const ms = Math.floor((time % 1000) / 10)
    .toString(10)
    .padStart(2, "0");
  const m = Math.floor((time / 60000) % 60)
    .toString(10)
    .padStart(2, "0");
  const s = Math.floor((time / 1000) % 60)
    .toString(10)
    .padStart(2, "0");

  // add hours part only if there are actually hours defined
  const hrs = h.toString(10).padStart(2, "0");
  const textStyles = [
    styles.timePart,
    isVertical ? styles.textVertical : styles.textHorizontal,
  ];
  return (
    <View style={[styles.formattedTime]}>
      {h > 0 && [
        <Text key="hour" style={textStyles}>
          {hrs}
        </Text>,
        <Sep
          key="hour-sep"
          style={isVertical ? styles.textVertical : styles.textHorizontal}
        >
          :
        </Sep>,
      ]}
      <Text style={textStyles}>{m}</Text>
      <Sep style={isVertical ? styles.textVertical : styles.textHorizontal}>
        :
      </Sep>
      <Text style={textStyles}>{s}</Text>
      <Sep style={isVertical ? styles.textVertical : styles.textHorizontal}>
        .
      </Sep>
      <Text style={textStyles}>{ms}</Text>
    </View>
  );
};

FormatTime.propTypes = {
  time: PropTypes.number,
};

const styles = StyleSheet.create({
  textVertical: {
    fontSize: "20vw",
  },
  textHorizontal: {
    fontSize: "11vw",
  },
  timePart: {
    fontVariantNumeric: "tabular-nums",
    color: "#ffffff",
    fontWeight: "600",
  },
  formattedTime: {
    flexDirection: "row",
    alignItems: "center",
    overflow: "hidden",
  },
});

export default FormatTime;
