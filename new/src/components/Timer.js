import React from "react";
import { View, StyleSheet } from "react-native";
import { useMachine } from "@xstate/react";
import FormatTime from "./FormatTime";
import TimerActions from "./TimerActions";
import { TimerType, timerStates } from "../stateMachines/timerMachine";

function Timer({ machine, isVertical }) {
  const [state, send] = useMachine(machine, { devTools: true });
  const { elapsed, timerType, duration, countdown } = state.context;
  let time = elapsed;
  if (timerType !== TimerType.StopWatch) {
    time = duration - elapsed;
  }
  if (state.matches(timerStates.idle)) {
    time = countdown;
  }
  console.log(time);
  return (
    <View style={[styles.container, !isVertical && styles.containerHorizontal]}>
      <View>
        <FormatTime time={time} isVertical={isVertical} />
        <TimerActions state={state} send={send} isVertical={isVertical} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  containerHorizontal: {
    maxWidth: "50%",
    maxHeight: "100%",
  },
  container: {
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    flex: 1,
    maxHeight: "20%",
  },
});

export default Timer;
