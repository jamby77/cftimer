import React, { useContext, useEffect, useCallback } from "react";
import { TimerType } from "../stateMachines/timerMachine";
import {
  View,
  Text,
  StyleSheet,
  TouchableHighlight,
  TextInput,
  Button,
} from "react-native";
import { AppContext, AppActions, displayTimer } from "../AppContext";
import TimePicker from "./TimePicker";

const initialTimer = {
  countdown: 3000,
  duration: 30000,
};
const { StopWatch, CountDown } = TimerType;

export default function TimerSelector({
  onPress,
  children,
  type = TimerType.StopWatch,
  selected = true,
}) {
  const { dispatch, workout, ...context } = useContext(AppContext);
  useEffect(() => {
    if (!selected) {
      return;
    }
    dispatch({ type: AppActions.timer, data: { ...initialTimer, type } });
  }, [type, dispatch, selected]);

  const handleCountdownChange = useCallback(
    countdown => {
      dispatch({
        type: AppActions.timer,
        data: { ...context.timer, countdown },
      });
    },
    [context.timer, dispatch],
  );
  const handleTimeChange = useCallback(
    time => {
      dispatch({
        type: AppActions.timer,
        data: { ...context.timer, duration: time },
      });
    },
    [context.timer, dispatch],
  );
  const handleWorkoutChange = useCallback(
    event => {
      dispatch({ type: AppActions.workout, data: event.target.value });
    },
    [dispatch],
  );

  const renderTimerInputs = (timer = {}) => {
    if (!timer) {
      return null;
    }
    const { countdown, duration } = timer || {};
    const result = [
      <View key="title">
        <Text style={[styles.text, styles.title]}>Initial countdown</Text>
        <TimePicker time={countdown} onChange={handleCountdownChange} />
      </View>,
    ];
    if (type === StopWatch) {
      result.push(
        <View key="cap">
          <Text style={[styles.text, styles.title]}>Time cap</Text>
          <TimePicker time={duration} onChange={handleTimeChange} />
        </View>,
      );
    }
    if (type === CountDown) {
      result.push(
        <View key="duration">
          <Text style={[styles.text, styles.title]}>AMRAP/Duration</Text>
          <TimePicker time={duration} onChange={handleTimeChange} />
        </View>,
      );
    }
    result.push(
      <View key="workout">
        <Text style={[styles.text, styles.title]}>Workout</Text>
        <TextInput
          style={styles.workout}
          numberOfLines={10}
          multiline
          maxLength={500}
          onChange={handleWorkoutChange}
          placeholder="Workout description"
          value={workout}
        />
        <View style={styles.medium}>
          <Button
            title="Done"
            onPress={() => {
              dispatch({ type: AppActions.display, data: displayTimer });
            }}
          />
        </View>
      </View>,
    );
    return result;
  };
  return (
    <View style={styles.form}>
      <TouchableHighlight onPress={onPress} style={styles.item}>
        <Text style={[styles.text, styles.title]}>{children}</Text>
      </TouchableHighlight>
      {selected && renderTimerInputs(context.timer)}
    </View>
  );
}

const styles = StyleSheet.create({
  title: {
    fontSize: "1em",
    textAlign: "center",
    textTransform: "uppercase",
    padding: "5px",
  },
  text: {
    color: "#ffffff",
  },
  workout: {
    backgroundColor: "white",
  },
  medium: {
    paddingTop: "5px",
    maxWidth: "300px",
    width: "300px",
    alignSelf: "center",
  },
  form: {
    textAlign: "center",
    paddingTop: "50px",
    maxWidth: "300px",
    width: "300px",
    alignSelf: "center",
  },
  item: {
    backgroundColor: "#8abdf0",
  },
});
