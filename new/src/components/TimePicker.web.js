import React, { useState, useEffect } from "react";
import { View } from "react-native";
import "rmc-picker/assets/index.css";
import MultiPicker from "rmc-picker/es/MultiPicker";
import Picker from "rmc-picker/es/Picker";

import utils from "./timePickerUtils";
import "./timePicker.css";

const { parseTime, maxTime, styles, minutes, seconds } = utils;

const TimePicker = ({ onChange = time => undefined, time = 0 }) => {
  const [value, setValue] = useState(parseTime(time));

  useEffect(() => {
    const timeToUpdate = (value[0] * 60 + value[1]) * 1000; // minutes plus seconds to milliseconds
    if (timeToUpdate === time) {
      return;
    }
    const update = Math.max(Math.min(timeToUpdate, maxTime), 0);
    onChange(update);
  }, [onChange, time, value]);

  return (
    <View style={styles.container}>
      <MultiPicker
        className="cftimer-picker"
        selectedValue={value}
        onValueChange={newValue => {
          setValue(newValue.map(Number));
        }}
      >
        <Picker
          className="cftimer-picker-item"
          indicatorClassName="cftimer-picker-indicator"
        >
          {minutes.map((_, idx) => {
            return (
              <Picker.Item key={idx} value={idx}>
                {`${idx.toString().padStart(2, "0")} m`}
              </Picker.Item>
            );
          })}
        </Picker>
        <Picker
          className="cftimer-picker-item"
          indicatorClassName="cftimer-picker-indicator"
        >
          {seconds.map((_, idx) => {
            return (
              <Picker.Item key={idx} value={idx}>
                {`${idx.toString().padStart(2, "0")} s`}
              </Picker.Item>
            );
          })}
        </Picker>
      </MultiPicker>
    </View>
  );
};

export default TimePicker;
