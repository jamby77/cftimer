import React from "react";
import FormatTime from "../components/FormatTime";

export default {
  title: "CFTimer/components/FormatTime",
  component: FormatTime,
};

export const HundredMs = () => <FormatTime time={100} />;
export const ThousandMs = () => <FormatTime time={1000} />;
export const TenThousandMs = () => <FormatTime time={10000} />;
export const HundredThousandMs = () => <FormatTime time={100000} />;
export const MillionMs = () => <FormatTime time={1000000} />;

HundredMs.story = { name: "100 ms" };
ThousandMs.story = { name: "1000 ms" };
TenThousandMs.story = { name: "10000 ms" };
HundredThousandMs.story = { name: "100000 ms" };
MillionMs.story = { name: "1000000 ms" };
