import React from "react";
import Timer from "../components/Timer";
import { TimerType, timerMachine } from "../stateMachines/timerMachine";

export default {
  title: "CFTimer/components/Timer",
  component: Timer,
};
const getMachine = timer =>
  timerMachine.withContext({
    ...timerMachine.context,
    timers: [{ ...timer, timerType: timer.type }],
    countdown: timer.countdown + 1,
  });

export const Countdown = () => {
  const timer = { type: TimerType.CountDown, duration: 10000, countdown: 3000 };
  const concreteMachine = getMachine(timer);

  return <Timer machine={concreteMachine} isVertical={false} />;
};

export const Stopwatch = () => {
  const timer = { type: TimerType.StopWatch, duration: 10000, countdown: 3000 };
  const concreteMachine = getMachine(timer);

  return <Timer machine={concreteMachine} isVertical={false} />;
};
