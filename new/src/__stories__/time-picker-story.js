import React from "react";
import TimePicker from "../components/TimePicker.web";
import TimePickerNative from "../components/TimePicker.native";
import { action } from "@storybook/addon-actions";
export default {
  title: "CFTimer/components/TimePicker",
  component: TimePicker,
};

export const normal = () => <TimePicker onChange={action("time changed")} />;
export const withTime = () => (
  <TimePicker onChange={action("time changed")} time={125000} />
);

export const normalNative = () => (
  <TimePickerNative onChange={action("time changed")} />
);
export const withTimeNative = () => (
  <TimePickerNative onChange={action("time changed")} time={125000} />
);
