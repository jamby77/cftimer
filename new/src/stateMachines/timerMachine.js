import { Machine, assign, actions } from "xstate";
const { log } = actions;

const idle = "idle";
const running = "running";
const paused = "paused";
const finished = "finished";
const reset = "reset";
export const timerStates = {
  idle,
  running,
  paused,
  finished,
  reset,
};

const START = "START";
const STOP = "STOP";
const PAUSE = "PAUSE";
const RESET = "RESET";
const FINISH = "FINISH";
const TICK = "TICK";
const NEXT = "NEXT";
export const timerTransitions = {
  START,
  STOP,
  PAUSE,
  RESET,
  FINISH,
  NEXT,
};

const StopWatch = "STOP_WATCH",
  Timer = "TIMER",
  Interval = "INTERVAL",
  ComplexInterval = "COMPLEX_INTERVAL",
  Rest = "REST",
  CountDown = "COUNTDOWN",
  Work = "WORK",
  Idle = "IDLE";
export const TimerType = {
  StopWatch,
  Timer,
  Interval,
  ComplexInterval,
  Rest,
  Work,
  CountDown,
};

function start(context) {
  return callback => {
    const rAFCallback = () => {
      if (context.timeStart === undefined) {
        context.timeStart = Date.now();
      }
      callback(TICK);
      context.rafId = requestAnimationFrame(rAFCallback);
    };
    context.rafId = requestAnimationFrame(rAFCallback);
  };
}

function pause(context) {
  return callback => {
    const rAFCallback = () => {
      context.pauseStart = Date.now() - context.pausedFor;
      if (context.rafId) {
        cancelAnimationFrame(context.rafId);
        context.rafId = undefined;
      }
      callback(TICK);
      context.rafId = requestAnimationFrame(rAFCallback);
    };
    context.rafId = requestAnimationFrame(rAFCallback);
    return () => (context.pauseStart = undefined);
  };
}

export const timerMachine = Machine({
  id: "timer",
  initial: timerStates.idle,
  context: {
    duration: Number.MAX_SAFE_INTEGER,
    elapsed: 0,
    timeStart: undefined,
    pauseStart: undefined,
    pausedFor: 0,
    rafId: undefined,
    countdown: 3000,
    timerType: Idle,
    timers: [],
    originalTimers: [],
    completeTimers: [],
  },
  entry: log("timer machine started"),
  exit: log("timer machine exited"),
  states: {
    [idle]: {
      invoke: {
        src: context => () => {
          console.log("idle entry");
          if (context.originalTimers.length === 0) {
            // original timers not set, assign them
            context.originalTimers = [...context.timers];
          }
          context.completeTimers = []; // reset complete timers
          // if (context.countdown) {
          context.duration = context.countdown;
          // }
        },
      },
      on: {
        [START]: running,
      },
    },
    [reset]: {
      entry: assign(context => {
        console.log("full reset");
        if (context.rafId) {
          cancelAnimationFrame(context.rafId);
        }
        return {
          elapsed: 0,
          pausedFor: 0,
          timeStart: Date.now(),
          pauseStart: undefined,
          timers: [...context.originalTimers],
          completeTimers: [],
        };
      }),
      exit: context => console.log(context),
      on: {
        "": idle,
      },
    },
    [running]: {
      initial: "countdown",
      states: {
        countdown: {
          entry: context => {
            console.log("countdown");
            context.timerType = CountDown;
            context.duration = context.countdown;
          },
          exit: () => console.log("exit countdown state"),
          on: {
            [TICK]: {
              target: "transition",
              cond: context => {
                return context.elapsed >= context.countdown;
              },
            },
          },
        },
        transition: {
          entry: context => {
            context.elapsed = 0;
            context.pausedFor = 0;
            context.timeStart = Date.now();
            context.pauseStart = undefined;
          }, // reset timer state and transition to next timer
          on: {
            "": "timer",
          },
        }, // queue next timer, transition to timer
        timer: {
          entry: assign(context => {
            if (context.timers.length === 0) {
              return;
            }
            const { timers } = context;
            const [nextTimer, ...restTimers] = timers;
            const newContext = {
              ...context,
              ...nextTimer,
              completeTimers: [...context.completeTimers, nextTimer],
              timers: restTimers,
            };
            return newContext;
          }),
        },
      },
      invoke: {
        src: start,
      },
      on: {
        "": {
          target: finished,
          cond: context => {
            console.log("testing for finishing");
            return (
              context.timers.length === 0 && context.elapsed >= context.duration
            );
          },
        },
        [TICK]: {
          actions: assign({
            elapsed: context => {
              console.log("tick from running", context.elapsed);
              return Date.now() - context.timeStart - context.pausedFor;
            },
          }),
        },
        [STOP]: reset,
        [PAUSE]: paused,
        [FINISH]: finished,
      },
    },
    [paused]: {
      invoke: { src: pause },
      on: {
        [TICK]: {
          actions: assign({
            pausedFor: context => Date.now() - context.pauseStart,
          }),
        },
        [START]: running,
        [RESET]: reset,
      },
    },
    [finished]: {
      invoke: {
        src: context => () => {
          context.elapsed = context.duration;
        },
      },
      on: {
        [RESET]: reset,
      },
    },
  },
});
