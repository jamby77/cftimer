import React, { Component } from "react";
import { Router } from "@reach/router";
import "./App.css";
import SetupTimer from "./components/SetupTimer";
import TimerStart from "./components/TimerStart";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
    console.error(error);
    console.log(info);
  }

  render() {
    if (this.state.hasError) {
      return <div className="error">Something went wrong</div>;
    }
    return (
      <div className="App">
        <div className="container text-center">
          <Router>
            <TimerStart path="start" />
            <SetupTimer default />
          </Router>
        </div>
      </div>
    );
  }
}

export default App;
