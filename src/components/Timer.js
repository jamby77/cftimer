/* eslint-disable react/prop-types */
import React, { Fragment, useEffect, useMemo, useRef, useState } from "react";
import PropTypes from "prop-types";
import { TimerType } from "../models/timer";
import FormatTime from "./helper/FormatTime";

const noopFunc = () => null;
const initialRefObject = {};

const useTimer = (timer, callbacks) => {
  // hook to run and manage a timer
  const [isRunning, setIsRunning] = useState(false);
  const [isComplete, setIsComplete] = useState(false);
  const [time, setTime] = useState(timer.getDuration() || 0);
  const intervalRef = useRef({});
  intervalRef.current.isRunning = isRunning;
  const { onPause, onReset, onComplete, onStart } = callbacks;

  const clearTick = () => {
    if (intervalRef.current.rafId === undefined) {
      return;
    }
    cancelAnimationFrame(intervalRef.current.rafId);
    intervalRef.current = { ...initialRefObject };
  };

  const tick = (tickComplete, initialTime = 0) => {
    const rAFCallback = () => {
      const now = Date.now();
      const { isRunning, time, pauseTime } = intervalRef.current;

      if (time === undefined) {
        intervalRef.current.time = now + initialTime;
      }

      if (isRunning) {
        let elapsedTime = now - time;
        if (pauseTime) {
          const tt = now - pauseTime; // time it has been paused
          elapsedTime -= tt; // subtract from elapsed time, paused time
          intervalRef.current.time += tt; // offset pause time from start
          intervalRef.current.pauseTime = undefined;
        }
        setTime(elapsedTime);

        if (tickComplete && tickComplete(elapsedTime)) {
          pauseTimer(true);
          setIsComplete(true);
          onComplete(timer);
          return;
        }
      } else {
        if (pauseTime === undefined) {
          intervalRef.current.pauseTime = now;
        }
      }
      intervalRef.current.rafId = requestAnimationFrame(rAFCallback);
    };
    intervalRef.current.rafId = requestAnimationFrame(rAFCallback);
  };

  const pauseTimer = (reset = false) => {
    console.log("pause called");
    setIsRunning(false);
    if (reset) {
      clearTick();
    }
    onPause(timer);
  };

  const reset = () => {
    pauseTimer(true);
    setTime(timer.getDuration() || 0);
    setIsComplete(false);
    onReset(timer);
  };

  const stopWatchTimer = () => {
    const tickComplete = elapsedTime => {
      const diff = timer.getCap() - elapsedTime;
      return diff <= 0;
    };
    tick(tickComplete);
  };

  const simpleTimer = () => {
    const tickComplete = elapsedTime => {
      return elapsedTime <= 0;
    };
    tick(tickComplete, timer.getDuration());
  };

  const startTimer = () => {
    console.log("startTimer called");
    setIsRunning(true);
    switch (timer.getTimerType()) {
      case TimerType.StopWatch:
        stopWatchTimer();
        break;
      case TimerType.Timer:
      case TimerType.Work:
      case TimerType.Rest:
        simpleTimer();
        break;
      default:
        console.log("Unknown interval");
        break;
    }
    onStart(timer);
  };

  const start = () => {
    return startTimer();
  };

  const stop = () => {
    return pauseTimer();
  };

  return { time, isRunning, isComplete, start, stop, reset };
};

const buttonStyle = {
  flexBasis: "200px"
};
const Timer = ({
  timer,
  shouldRun = false,
  onReset = () => noopFunc,
  onComplete = () => noopFunc,
  onPause = () => noopFunc,
  onStart = () => noopFunc
}) => {
  const callbacks = useMemo(
    () => ({
      onStart,
      onComplete,
      onPause,
      onReset
    }),
    [onStart, onComplete, onPause, onReset]
  );

  const { start, stop, time, reset, isRunning, isComplete } = useTimer(
    timer,
    callbacks
  );

  useEffect(() => {
    shouldRun && start();
  }, []);

  const { styles } = timer.getTimer();

  const handleStartStopClick = () => {
    isRunning ? stop() : start();
  };
  return (
    shouldRun && (
      <div
        className="timer"
        style={{
          display: "flex",
          flexDirection: "column",
          minHeight: "100vh",
          justifyContent: "space-between",
          padding: "2rem",
          fontWeight: "800",
          ...styles
        }}
      >
        <div className="timer-description">
          <p>{timer.getDescription()}</p>
        </div>
        <div className="timer-content">
          <FormatTime time={time} />
        </div>
        <div
          className="timer-controls"
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          {!isComplete ? (
            <Fragment>
              <button
                className="btn btn-green"
                onClick={handleStartStopClick}
                style={buttonStyle}
              >
                {isRunning ? "Pause" : "Start"}
              </button>
              {isRunning ? null : (
                <button
                  style={buttonStyle}
                  className="btn btn-red"
                  onClick={() => {
                    reset();
                  }}
                >
                  Reset
                </button>
              )}
            </Fragment>
          ) : (
            <button
              style={buttonStyle}
              className="btn btn-blue"
              onClick={() => {
                reset();
                start();
              }}
            >
              Done, run again
            </button>
          )}
        </div>
      </div>
    )
  );
};

export default Timer;

Timer.propTypes = {
  timer: PropTypes.object.isRequired,
  onComplete: PropTypes.func,
  onPause: PropTypes.func,
  onReset: PropTypes.func,
  onStart: PropTypes.func,
  shouldRun: PropTypes.bool
};
