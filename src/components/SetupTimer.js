import React, { useState } from "react";
import PropTypes from "prop-types";

import { addTimers, timers } from "../TimerContext";
import Timer, { TimerType } from "../models/timer";
import SimpleIntervalForm from "./form/SimpleIntervalForm";
import StopWatchForm from "./form/StopWatchForm";
import ComplexTimerForm from "./form/ComplexTimerForm";
import TimerForm from "./form/TimerForm";

const defaultTimer = new Timer(TimerType.StopWatch);

const SetupTimer = ({ timerType, navigate }) => {
  const [type, setTimerType] = useState(parseInt(timerType, 10));
  const [config, setConfig] = useState({});

  const handleTimerStart = addTimers(timers);

  const handleTimerTypeChange = newTimerType => {
    if (newTimerType !== type) {
      setTimerType(newTimerType);
    }
    // if (newTimerType === TimerType.StopWatch) {
    //   setConfig({
    //     duration: 0
    //   });
    // }
  };

  const handleDurationChange = () => {
    return values => {
      console.log(values);
      const config =
        typeof values === "number" ? { duration: values } : { ...values };
      setConfig(config);
    };
  };

  const renderForm = timerType => {
    if (timerType === TimerType.StopWatch) {
      return (
        <div className="timer-section stopwatch">
          <StopWatchForm onChange={handleDurationChange()} />
        </div>
      );
    } else if (timerType === TimerType.Interval) {
      return (
        <div className="timer-section interval">
          <SimpleIntervalForm onChange={handleDurationChange()} />
        </div>
      );
    } else if (timerType === TimerType.Timer) {
      return (
        <div className="timer-section work">
          <TimerForm onChange={handleDurationChange()} />
        </div>
      );
    }
    return <ComplexTimerForm onChange={handleDurationChange()} />;
  };

  const timerTypesOptions = () => {
    return Object.values(TimerType).filter(
      type => type !== TimerType.Work && type !== TimerType.Rest
    );
  };

  const showStart =
    Object.keys(config).length > 0 || type === TimerType.StopWatch;
  return (
    <div>
      {timerTypesOptions().map(typeOption => (
        <div key={typeOption}>
          <button
            className="btn"
            onClick={() => handleTimerTypeChange(typeOption)}
          >
            {defaultTimer.getTimerTypeLabel(typeOption)}
          </button>
          {typeOption === type && renderForm(type)}
        </div>
      ))}

      {showStart && (
        <button
          className="btn btn-blue btn-start"
          onClick={() => {
            handleTimerStart({ config, timerType: type });
            navigate("/start");
          }}
        >
          Start
        </button>
      )}
    </div>
  );
};

SetupTimer.propTypes = {
  timerType: PropTypes.number,
  navigate: PropTypes.func
};

SetupTimer.defaultProps = {
  timerType: TimerType.StopWatch
};
export default SetupTimer;
