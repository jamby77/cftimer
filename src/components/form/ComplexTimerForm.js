import React, { useState } from "react";
import PropTypes from "prop-types";
import { TimerType } from "../../models/timer";
import SimpleIntervalForm from "./SimpleIntervalForm";
import TimeSelector from "../helper/TimeSelector";

const ComplexTimerForm = ({ onChange, containerClass }) => {
  const [timers, setTimers] = useState([]);

  const addTimer = type => {
    const now = new Date();
    return () => {
      let timer;
      if (type === TimerType.Interval) {
        timer = {
          label: "Interval",
          type,
          key: "int" + now.getTime()
        };
      } else if (type === TimerType.Work) {
        timer = {
          label: "Work",
          type,
          key: "work" + now.getTime()
        };
      } else if (type === TimerType.Rest) {
        timer = {
          label: "Rest",
          type,
          key: "rest" + now.getTime()
        };
      } else {
        throw new Error("Unknown timer type");
      }
      setTimers(timers => {
        let highestOrder =
          timers.length > 0 ? timers[timers.length - 1].order || 0 : 0;
        // make sure highest order is an integer
        timer.order = parseInt(highestOrder, 10) + 1;
        // additional timer config, such as styling, duration etc
        timer.config = {};
        return [...timers, timer];
      });
    };
  };

  const removeTimer = () => {};

  const handleDurationChange = timer => {
    return value => {
      let config = { ...timer.config };
      if (
        timer.type === TimerType.Timer ||
        timer.type === TimerType.Work ||
        timer.type === TimerType.Rest
      ) {
        config.duration = value;
      } else if (timer.type === TimerType.Interval) {
        config = value;
      }

      setTimers(timers => {
        const currentTimer = timers.find(t => {
          return t.order === timer.order;
        });
        if (!currentTimer) {
          throw new Error("Could not find timer");
        }
        currentTimer.config = config;

        const newState = [...timers];
        onChange(newState);
        return newState;
      });
    };
  };

  const renderTimer = timer => {
    if (timer.type === TimerType.Interval) {
      return (
        <div className="complex-interval-section interval" key={timer.key}>
          <SimpleIntervalForm onChange={handleDurationChange(timer)} />
        </div>
      );
    } else {
      return (
        <div
          className={`complex-interval-section ${timer.label.toLowerCase()}`}
          key={timer.key}
        >
          <TimeSelector
            containerClass={containerClass}
            onChange={handleDurationChange(timer)}
          >
            {timer.label}
          </TimeSelector>
        </div>
      );
    }
  };

  return (
    <div className="container text-center">
      <div className="complex-timer-menu mt-4">
        <h3>Complex timer</h3>
        <div className={containerClass}>
          <button
            className="btn btn-blue w-48"
            onClick={addTimer(TimerType.Interval)}
          >
            Add Interval
          </button>
        </div>
        <div className={containerClass}>
          <button
            className="btn btn-green w-48"
            onClick={addTimer(TimerType.Work)}
          >
            Add Work
          </button>
        </div>
        <div className={containerClass}>
          <button
            className="btn btn-red w-48"
            onClick={addTimer(TimerType.Rest)}
          >
            Add Rest
          </button>
        </div>
      </div>
      <div className="complex-timer-timer">
        {timers.map(t => {
          return renderTimer(t);
        })}
      </div>
    </div>
  );
};

ComplexTimerForm.propTypes = {
  containerClass: PropTypes.string,
  onChange: PropTypes.func.isRequired
};

export default ComplexTimerForm;
