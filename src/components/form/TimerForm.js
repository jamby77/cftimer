import React from "react";
import PropTypes from "prop-types";
import TimeSelector from "../helper/TimeSelector";

const TimerForm = ({ containerClass, onChange }) => {
  return (
    <TimeSelector containerClass={containerClass} onChange={onChange}>
      Set Timer Duration
    </TimeSelector>
  );
};

TimerForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  containerClass: PropTypes.string
};

export default TimerForm;
