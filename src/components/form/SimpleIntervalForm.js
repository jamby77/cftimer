import React, { Fragment, useRef, useState } from "react";
import PropTypes from "prop-types";
import TimeSelector from "../helper/TimeSelector";
import RepeatsSelector from "../helper/RepeatsSelector";

const SimpleIntervalForm = ({ onChange }) => {
  const repeatRef = useRef(null);
  const [, setState] = useState({
    work: 0,
    rest: 0,
    repeat: 1
  });

  const getRepeats = () => {
    return repeatRef.current.value - 0;
  };

  const handleRepeatChange = () => {
    setState(state => {
      const newState = {
        ...state,
        repeat: getRepeats()
      };
      onChange(newState);
      return newState;
    });
  };

  const handleWorkChange = workTime => {
    setState(state => {
      const newState = {
        ...state,
        work: workTime,
        repeat: getRepeats()
      };
      onChange(newState);
      return newState;
    });
  };

  const handleRestChange = restTime => {
    setState(state => {
      const newState = {
        ...state,
        rest: restTime,
        repeat: getRepeats()
      };
      onChange(newState);
      return newState;
    });
  };

  return (
    <Fragment>
      <TimeSelector onChange={handleWorkChange}>Work</TimeSelector>
      <TimeSelector onChange={handleRestChange}>Rest</TimeSelector>
      <div onBlur={handleRepeatChange}>
        <RepeatsSelector repeatRef={repeatRef}># of rounds</RepeatsSelector>
      </div>
    </Fragment>
  );
};

SimpleIntervalForm.propTypes = {
  onChange: PropTypes.func.isRequired
};

export default SimpleIntervalForm;
