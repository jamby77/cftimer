import React from "react";
import PropTypes from "prop-types";
import TimeSelector from "../helper/TimeSelector";

const StopWatchForm = ({
  containerClass = "control-container h-12",
  onChange
}) => {
  return (
    <TimeSelector containerClass={containerClass} onChange={onChange}>
      Set Time Cap(Optional)
    </TimeSelector>
  );
};

StopWatchForm.propTypes = {
  onChange: PropTypes.func.isRequired,
  containerClass: PropTypes.string
};
export default StopWatchForm;
