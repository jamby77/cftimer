import React, { Fragment, useState } from "react";
import Timer from "./Timer";
import PropTypes from "prop-types";
import { timers } from "../TimerContext";
import SetupTimer from "./SetupTimer";
import FormatTime from "./helper/FormatTime";

const TimerStart = ({ navigate }) => {
  const groupsDuration = {};
  const count = timers.timers.length;
  const [current, setCurrent] = useState(0);

  const groupConfig = timer => {
    const group = timer.getConfig("group");
    if (!group) {
      return {
        total: timer.getDuration(),
        remaining: timer.getDuration(),
        count: 1,
        current: 0
      };
    }
    if (!groupsDuration[group]) {
      const groupTimers = timers.timers.filter(
        timer => timer.getConfig("group") === group
      );
      const groupDuration = groupTimers.reduce((total, timer) => {
        return total + timer.getDuration();
      }, 0);
      groupsDuration[group] = {
        total: groupDuration,
        remaining: groupDuration,
        count: groupTimers.length,
        current: 0
      };
    }
    return groupsDuration[group];
  };

  const renderTimerHeader = timer => {
    const group = timer.getConfig("group");
    const gConfig = groupConfig(timer);
    return group ? (
      <Fragment>
        <h2>{`Running ${current + 1} of ${count} timers`}</h2>
        <h3>
          [{`Interval ${group}, total time:`},
          <FormatTime time={gConfig.total} />, {", remaining time: "},
          <FormatTime time={gConfig.remaining} />]
        </h3>
      </Fragment>
    ) : null;
  };

  const updateGroupConfig = timer => {
    const group = timer.getConfig("group");
    if (!group) {
      return;
    }
    const gConfig = groupConfig(timer);
    gConfig.remaining -= timer.getDuration();
  };

  if (!count) {
    navigate("/");
    return (
      <div>
        <h2>No timers to run</h2>
        <SetupTimer />
      </div>
    );
  }
  const timer = timers.timers[current];
  if (!timer) {
    navigate("/");
    return (
      <div>
        <h2>Done</h2>
        <SetupTimer />
      </div>
    );
  }

  return (
    <Fragment>
      {renderTimerHeader(timer)}
      <Timer
        timer={timer}
        key={current}
        shouldRun
        onComplete={timer => {
          timer.complete();
          updateGroupConfig(timer);
          setCurrent(current + 1);
        }}
      />
    </Fragment>
  );
};

TimerStart.propTypes = {
  navigate: PropTypes.func
};

export default TimerStart;
