import React, { useState } from "react";
import PropTypes from "prop-types";
import { formatTimePart as fp, range } from "../../models/timer";

const useTime = (initialTime = 0, onChange) => {
  const [time, setTime] = useState(Number(initialTime));

  const getMin = currentTime => {
    return !currentTime || isNaN(currentTime)
      ? 0
      : Math.floor((currentTime / 60000) % 60);
  };

  const getSec = currentTime => {
    return !currentTime ? 0 : Math.floor((currentTime / 1000) % 60);
  };

  const setMinutes = min => {
    // convert min to sec and emit on change
    setTime(currentTime => {
      const time = (Number(min) * 60 + getSec(currentTime)) * 1000; // to milliseconds
      onChange(time);
      return time;
    });
  };

  const setSeconds = sec => {
    setTime(currentTime => {
      const time = (Number(sec) + 60 * getMin(currentTime)) * 1000; // to milliseconds
      onChange(time);
      return time;
    });
  };

  return {
    minutes: getMin(time),
    seconds: getSec(time),
    setMinutes,
    setSeconds
  };
};

const TimeSelector = props => {
  const {
    children,
    containerClass = "control-container h-12",
    min = 0,
    max = 99,
    minLabel = "Minutes",
    secLabel = "Seconds",
    initialTime = 0,
    onChange = v => console.log(v)
  } = props;

  const time = useTime(initialTime, onChange);

  return (
    <div className={containerClass}>
      <h4>{children}</h4>
      <label>
        {minLabel}
        <select
          onChange={e => {
            time.setMinutes(e.target.value);
          }}
          value={time.minutes}
        >
          {range(min, max, 1).map(i => (
            <option key={`min-${i}`} value={i}>
              {fp(i)}
            </option>
          ))}
        </select>
      </label>
      <label>
        {secLabel}
        <select
          onChange={e => {
            time.setSeconds(e.target.value);
          }}
          value={time.seconds}
        >
          {range(min, max < 60 ? max : 59, 5).map(i => (
            <option key={`sec-${i}`} value={i}>
              {fp(i)}
            </option>
          ))}
        </select>
      </label>
    </div>
  );
};

TimeSelector.propTypes = {
  onChange: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  containerClass: PropTypes.string,
  initialTime: PropTypes.number,
  min: PropTypes.number,
  max: PropTypes.number,
  minLabel: PropTypes.string,
  secLabel: PropTypes.string
};
export default TimeSelector;
