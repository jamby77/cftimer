import React from "react";
import PropTypes from "prop-types";
import { formatTimePart as fp, range } from "../../models/timer";

const RepeatsSelector = ({
  children,
  containerClass = "control-container h-12",
  min = 1,
  max = 100,
  repeatRef
}) => {
  return (
    <div className={containerClass}>
      <h4>{children}</h4>
      <label>
        <select ref={repeatRef} defaultValue={min}>
          {range(min, max, 1).map(i => (
            <option key={`rnd-${i}`} value={i}>
              {fp(i)}
            </option>
          ))}
        </select>
      </label>
    </div>
  );
};

RepeatsSelector.propTypes = {
  children: PropTypes.node.isRequired,
  containerClass: PropTypes.string,
  min: PropTypes.number,
  max: PropTypes.number,
  repeatRef: PropTypes.object
};

export default RepeatsSelector;
