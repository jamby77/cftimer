import React from "react";
import PropTypes from "prop-types";

const Sep = ({ children }) => (
  <span className="time-separator">{children}</span>
);
Sep.propTypes = {
  children: PropTypes.string.isRequired
};

const FormatTime = ({ time }) => {
  // time is in milliseconds
  const h = Math.floor(time / 3600000);
  const ms = Math.floor((time % 1000) / 10)
    .toString(10)
    .padStart(2, "0");
  const m = Math.floor((time / 60000) % 60)
    .toString(10)
    .padStart(2, "0");
  const s = Math.floor((time / 1000) % 60)
    .toString(10)
    .padStart(2, "0");

  // add hours part only if there are actually hours defined
  const hrs = h.toString(10).padStart(2, "0");
  return (
    <div className="formatted-time flex justify-center">
      {h > 0 && [
        <span key="hour" className="hours time-part">
          {hrs}
        </span>,
        <Sep key="hour-sep">:</Sep>
      ]}
      <span className="minutes time-part">{m}</span>
      <Sep>:</Sep>
      <span className="seconds time-part">{s}</span>
      <Sep>.</Sep>
      <span className="ms time-part">{ms}</span>
    </div>
  );
};

FormatTime.propTypes = {
  time: PropTypes.number
};

export default FormatTime;
