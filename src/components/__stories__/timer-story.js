import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import "../../index.css";
import Timer from "../Timer";
import { addTimers, timers } from "../../TimerContext";
import { TimerType } from "../../models/timer";

storiesOf("CFTimer.components.Timer", module).add("Stopwatch timer", () => {
  const add = addTimers(timers);
  const config = { duration: 10000 },
    timerType = TimerType.StopWatch;
  add({ config, timerType });
  const timer = timers.timers.pop();
  const onComplete = action("Timer complete");
  const onPause = action("Timer paused");
  const onReset = action("Timer reset");
  const onStart = action("Timer started");
  const onChange = action("Timer changed");
  return (
    <React.Fragment>
      <pre>{JSON.stringify({ config, timer }, null, 4)}</pre>
      <Timer
        timer={timer}
        shouldRun
        onComplete={onComplete}
        onPause={onPause}
        onReset={onReset}
        onStart={onStart}
        onChange={onChange}
      />
    </React.Fragment>
  );
});
