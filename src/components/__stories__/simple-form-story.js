import React from "react";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import SimpleIntervalForm from "../form/SimpleIntervalForm";

storiesOf("CFTimer.components.SimpleIntervalForm", module).add(
  "Plain form",
  () => {
    return <SimpleIntervalForm onChange={action("Simple interval changed")} />;
  }
);
