import React from "react";
import { storiesOf } from "@storybook/react";
import FormatTime from "../helper/FormatTime";
import "../../index.css";

storiesOf("CFTimer.components.FormatTime", module)
  .add("100 ms", () => {
    return <FormatTime time={100} />;
  })
  .add("1000 ms", () => {
    return <FormatTime time={1000} />;
  })
  .add("10000 ms", () => {
    return <FormatTime time={10000} />;
  })
  .add("100000 ms", () => {
    return <FormatTime time={100000} />;
  })
  .add("1000000 ms", () => {
    return <FormatTime time={1000000} />;
  });
