import Timer, { TimerType } from "./models/timer";
const { Interval, StopWatch, ComplexInterval, Work, Rest } = TimerType;

export const timers = {
  timers: []
};
// group intervals
let globalGroupId = 0;

const generateIntervalTimers = (config, timers = []) => {
  ++globalGroupId;
  // given interval config, generate all work/rest timers
  const { work, rest, repeat } = config;
  for (let i = 0; i < repeat; i++) {
    timers.push(
      new Timer(Work, work, { description: "Work", group: globalGroupId })
    );
    timers.push(
      new Timer(Rest, rest, { description: "Rest", group: globalGroupId })
    );
  }
  // remove last rest
  timers.pop();
};

export const addTimers = timers => {
  return config => {
    if (!config.timerType) {
      console.log("Timer type missing: ", config);
      return;
    }

    const newTimers = [];
    switch (config.timerType) {
      case ComplexInterval:
        const complexTimers = config.config.timers;
        complexTimers.sort((a, b) => {
          return a.order - b.order;
        });
        complexTimers.forEach(function(t) {
          // prepare all timers in complex interval
          // console.log(t);
          switch (t.type) {
            case Interval:
              generateIntervalTimers(t.config, newTimers);
              break;
            case Work:
            case Rest:
              t.config.duration > 0 &&
                newTimers.push(
                  new Timer(t.type, t.config.duration, {
                    description: t.type === Work ? "Work" : "Rest"
                  })
                );
              break;
            default:
              throw new TypeError("Unknown timer type");
          }
        });
        break;
      case Interval:
        generateIntervalTimers(config.config, newTimers);
        break;
      case Timer:
      case StopWatch:
      default:
        newTimers.push(new Timer(config.timerType, config.config.duration));
        break;
    }
    timers.timers = [...timers.timers, ...newTimers];
  };
};
