const TimerType = {
  StopWatch: 1,
  Timer: 2,
  Interval: 3,
  ComplexInterval: 4,
  Rest: 5,
  Work: 6
};
const maxTime = 365 * 24 * 3600; // max supported timer is for 365 days
const styles = {
  [TimerType.Timer]: {
    color: "#4299e1",
    backgroundColor: "#fff",
    fontSize: "2em",
    fontWeight: "bold"
  },
  [TimerType.Rest]: {
    color: "#f56565",
    backgroundColor: "#fff",
    fontSize: "2em",
    fontWeight: "bold"
  },
  [TimerType.Work]: {
    color: "#22543d",
    backgroundColor: "#fff",
    fontSize: "2em",
    fontWeight: "bold"
  }
};
const timer = {
  type: TimerType.StopWatch,
  duration: 0,
  styles: {
    color: "#000000",
    backgroundColor: "#ffffff",
    fontSize: "2em",
    fontWeight: "bold"
  }
};
const formatTimePart = part => (part >= 10 ? part : "0" + part);
const range = (start, finish, step = 1, exclusive = false) => {
  const arr = [];
  start = parseInt(start, 10);
  finish = parseInt(finish, 10);
  if (isNaN(start) || isNaN(finish) || start >= finish) {
    return arr;
  }

  if (exclusive) {
    // if exclusive, make range open
    finish--;
  }

  while (start <= finish) {
    arr.push(start);
    start += step;
  }
  return arr;
};

const timerTypeLabel = type => {
  switch (type) {
    case TimerType.StopWatch:
      return "Stopwatch / For Time";
    case TimerType.Timer:
      return "Countdown / AMRAP";
    case TimerType.Interval:
      return "Interval / EMOM";
    case TimerType.ComplexInterval:
      return "Complex Interval";
    default:
      return "Unknown";
  }
};
const types = Object.values(TimerType);

export default class Timer {
  complete() {
    this.getTimer().complete = true;
  }

  isComplete() {
    return !!this.getTimer().complete;
  }

  getConfig(key) {
    return this.getTimer()[key];
  }

  setDuration(duration) {
    const durationInt = parseInt(duration, 10);
    if (isNaN(durationInt)) {
      console.warn(`Duration is not a number: ${duration}`);
      return;
    } else if (durationInt > maxTime) {
      console.warn(
        `Duration is more than allowed. Max allowed is 365 days, ${duration} provided`
      );
      return;
    }
    if (this.getTimerType() === TimerType.StopWatch) {
      this.timer.cap = duration;
    } else {
      this.timer.duration = duration;
    }
  }

  setType(type) {
    const idx = types.find(i => i === type);
    if (!idx) {
      console.warn(`Unsupported timer type: ${type}`);
      return;
    }
    this.timer.type = type;
  }

  getTimerType() {
    return this.timer.type;
  }

  getDescription() {
    return this.getTimer().description || this.getTimerTypeLabel();
  }

  getTimerTypeLabel(type) {
    return type === undefined
      ? timerTypeLabel(this.timer.type)
      : timerTypeLabel(type);
  }

  setStyles(styles) {
    this.timer.styles = styles;
  }

  getTimer() {
    return this.timer;
  }

  getDuration() {
    return this.timer.duration;
  }

  getCap() {
    return this.timer.cap || 0;
  }

  constructor(type, duration, config = {}) {
    this.timer = { ...timer, ...config };
    this.setType(type);
    this.setDuration(duration);
    this.setStyles(styles[type]);
  }
}

export { TimerType, formatTimePart, range };
